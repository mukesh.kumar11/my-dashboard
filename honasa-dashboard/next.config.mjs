/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
          {
            protocol: 'https',
            hostname: 'images.mamaearth.in',
          },
          {
            protocol: 'https',
            hostname: 'a0.awsstatic.com',
          },
        ],
      },
};

export default nextConfig;

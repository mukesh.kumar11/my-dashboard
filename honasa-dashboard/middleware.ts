import { NextRequest, NextResponse,  userAgent } from 'next/server'

export function middleware(request: NextRequest) {
  const { device } = userAgent(request)
  const isMobile = device.type === 'mobile' ? '1' : '0';
  const response = NextResponse.next();
  response.headers.set('isMobile', isMobile);
  return response;
}

export const config = {
  matcher: ['/:path*', '/dashboard/:path*'],
}
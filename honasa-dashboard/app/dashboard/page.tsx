import { headers } from "next/headers";
import DashbaordDesktop from "../modules/dashboard/desktop";
import DashbaordMobile from "../modules/dashboard/components/mobile";

const HomePage = (): JSX.Element => {
  const headerlist = headers();
  const isMobile = headerlist.get("isMobile") == "1" ? true : false;

  if (isMobile) {
    return <DashbaordMobile />;
  }

  return <DashbaordDesktop isMobile={isMobile}/>;
};



export default HomePage;

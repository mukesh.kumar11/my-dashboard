import styled from "styled-components";

interface PrimaryButtonProps {
  className?: string;
  textClassName?: string;
  text: string;
  onClick?: () => void
}

const PrimaryButtonLayout = styled.button`
  border: 1px solid;
  border-color: #109E90;
  background-color: #109E90;
  border-radius: 8px;
  padding: 10px 34px;
  width: 100%;
  cursor: pointer;

  .text {
    color: #FFFFFF;
    font-size: 18px;
    font-weight: 400;
    letter-spacing: 0;
    line-height: 30px;
    width: 100%;
    text-align: center;
  }
`;

export const PrimaryButton = ({
  className = '',
  textClassName = '',
  text = "Button",
  onClick = () => { console.log("No Event is passed")}
}: PrimaryButtonProps): JSX.Element => {
  return (
    <PrimaryButtonLayout className={className} onClick={onClick}>
      <p className={`text ${textClassName}`}>{text}</p>
    </PrimaryButtonLayout>
  );
};

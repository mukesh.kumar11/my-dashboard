import styled from "styled-components";

interface SecondaryButtonProps {
  className?: string;
  textClassName?: string;
  text: string;
  onClick?: () => void
}

const ButtonSecondaryLayout = styled.div`
  border: 1px solid;
  border-color: #000000;
  border-radius: 8px;
  padding: 10px 48px;
  cursor: pointer;
  text-align: center;
  .text {
    color: #000000;
    font-size: 18px;
    font-weight: 400;
    letter-spacing: 0;
    line-height: 30px;
    text-align: center;
  }
`;

export const SecondaryButton = ({
  className = '',
  textClassName = '',
  text = "Button",
  onClick = () => {}
}: SecondaryButtonProps): JSX.Element => {
  return (
    <ButtonSecondaryLayout className={className} onClick={onClick}>
      <span className={`text ${textClassName}`}>{text}</span>
    </ButtonSecondaryLayout>
  );
};

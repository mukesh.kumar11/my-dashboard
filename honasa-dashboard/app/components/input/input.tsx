import Image from "next/image";
import React from "react";
import styled from "styled-components";

const InputFieldLayout = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  gap: 10px;
  width: 100%;
  border: 1px solid #1010101a;
  border-radius: 10px;
      /* &:focus {
      border: 1px solid #11b8a8;
    } */
  & .input-field {
    background: white;
    border: none;
    outline: none;
    padding: 12px;
    font-size: 18px;
    color: black;
    border-radius: 8px;
    outline: none;
  }
`;

type InputFieldProps = {
  type?: string;
  id?: string;
  className?: string;
  name?: string;
  label?: string;
  labelClassName?: string;
  placeholder?: string;
  value?: string;
  required?: boolean;
  onChange?: (param: any) => void;
};

export const InputField = ({
  type = "text",
  id,
  className = "input-field-group",
  name,
  label,
  labelClassName,
  placeholder = "",
  value,
  required,
  onChange,
}: InputFieldProps) => {
  return (
    <InputFieldLayout className={className}>
      <Image
          src="https://images.mamaearth.in/wysiwyg/searchPageImages/searchPath.svg"
          alt="Honasa"
          width={24}
          height={24}
          style={{
            aspectRatio: 1686 / 1313,
            marginLeft: '12px',
          }}
          loader={({ src }) => src}
        />
      <input
        className={`input-field ${className}`}
        id={id}
        type={type}
        name={name}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        required={required}
      />
    </InputFieldLayout>
  );
};

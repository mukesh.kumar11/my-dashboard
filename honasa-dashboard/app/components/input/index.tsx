import React from "react";
import styled from "styled-components";

const InputFieldLayout = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 56px;
  margin: auto;
  position: relative;
  margin-top: 30px;
  
  & .input-field {
    width: 100%;
    height: 100%;
    background-color: transparent;
    border: none;
    padding-top: 20px;
    outline: none;
    font-size: 18px;
    color: black !important;

    &:focus + .label{
      border: none;
    }

    &:focus + .label .input-label{
      transform: translateY(-150%);
      color: #11b8a8;
      font-size: 14px;
      border: none;
    }
    

    &:focus + .label::after{
      transform: translateX(0%);
      transition: all 0.3s ease-in;
    }

    &:not(:placeholder-shown) + .label .input-label {
      transform: translateY(-150%);
      color: #11b8a8;
      font-size: 14px;
      border: none;
    }
  }

  & .label {
    position: absolute;
    left: 0px;
    width: 100%;
    height: 100%;
    bottom: 0px;
    color: #1010106a;
    font-size: 18px;
    border-bottom: 1px solid #1010106a;
    overflow-x: hidden;
    pointer-events: none;
    &::after {
      content: "";
      position: absolute;
      left: 0px;
      width: 100%;
      height: 100%;
      bottom: -1px;
      border-bottom: 3px solid #11b8a8;
      transform: translateX(-100%);
    }

    & .input-label {
      position: absolute;
      left: 0px;
      bottom: 6px;
      transition: all 0.3s ease-in;
    }
  }
`;

type InputFieldProps = {
  type?: string;
  id?: string;
  className?: string;
  name?: string;
  label?: string;
  labelClassName?: string;
  onChange?: (e: { target: { value: string; }; }) => Promise<void>,
  required?: boolean
};

export const InputField = ({
  type = "text",
  id,
  className = "input-field-group",
  name,
  label,
  labelClassName,
  onChange,
  required
}: InputFieldProps) => {
  return (
    <InputFieldLayout className={className}>
      <input
        className={`input-field ${className}`}
        id={id}
        type={type}
        name={name}
        placeholder=""
        onChange={onChange}
        required={required}
      />
      <label htmlFor={id} className={`label ${labelClassName}`}>
        <span className="input-label">{label}</span>
      </label>
    </InputFieldLayout>
  );
};
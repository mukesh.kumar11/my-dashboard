"use client";

import { InputField } from "honasa/app/components/input/input";
import { useState } from "react";
import styled from "styled-components";

const DashboardHeaderLayout = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  padding: 12px 42px 12px 24px;
  box-shadow: rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px;

  .left-container {
    width: 70%;

    .search-box {
      width: 420px;
    }
  }

  .right-container {
    width: 30%;
  }
`;

interface DashboardHeaderProps {
  isMobile: boolean,
  handleSearch: (e: { target: { value: string; }; }) => Promise<void>
}

const DashboardHeader = ({ isMobile, handleSearch }: DashboardHeaderProps) => {
  return (
    <DashboardHeaderLayout>
      <div className="left-container">
        <div className="search-box">
          <InputField
            type="text"
            id="search"
            className="input-field-group"
            name="search"
            placeholder="Search Honasa Tools"
            onChange={handleSearch}
          />
        </div>
      </div>
      <div className="right-container"></div>
    </DashboardHeaderLayout>
  );
};

export default DashboardHeader;

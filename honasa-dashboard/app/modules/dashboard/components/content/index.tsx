import Image from "next/image";
import Link from "next/link";
import styled from "styled-components";

interface DashboardContent {
  isMobile: boolean;
}

const toolList = [
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "Google Cloud",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "Cloudfront",
    owner: "Mukesh Kumar",
    email: "mukesh.kumar@mamaearth.in",
    phone: "7004964630",
    link: "https://dash.cloudflare.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
  {
    logo: "https://a0.awsstatic.com/libra-css/images/site/fav/favicon.ico",
    tool: "AWS",
    owner: "Naman Vyas",
    email: "naman.v@mamaearth.in",
    phone: "9876543210",
    link: "https://aws.amazon.com/",
  },
];

interface DashboardContentProps {
  isMobile: boolean;
}

const DashboardContentLayout = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  .heading {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px,
      rgba(0, 0, 0, 0.3) 0px 30px 60px -30px;
    height: 64px;
    width: 100%;
    background-color: wheat;
    padding: 24px;

    .title {
      padding: 12px;
      font-weight: 600;
    }
    .title-no {
      width: 32px;
    }
    .title-logo {
      width: 56px;
    }
    .title-tool-name {
      width: 160px;
    }

    .title-owner {
      width: 200px;
    }

    .title-email {
      width: 200px;
    }

    .title-link {
      width: 100px;
    }
  }

  .body-content {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    gap: 12px;
    width: 100%;
    background-color: white;
    padding: 24px;

    .tool-wrap {
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
      width: 100%;
      border-bottom: 2px solid gray;

      .content {
        padding: 12px;
      }

      .title {
        padding: 12px;
      }

      .sno {
        width: 32px;
      }

      .logo {
        width: 42px;
        height: 42px;
      }

      .name {
        width: 160px;
      }

      .owner {
        width: 200px;
      }

      .email {
        width: 200px;
      }

      .visit-link {
        width: 100px;
      }
    }
  }
`;

const DashboardContent = ({ isMobile, query }: DashboardContentProps) => {
  return (
    <DashboardContentLayout>
      <div className="heading">
        <div className="title title-no">No</div>
        <div className="title title-logo">Logo</div>
        <div className="title title-tool-name">Tool Name</div>
        <div className="title title-owner">Owner</div>
        <div className="title title-owner">Email</div>
        <div className="title title-link">Visit Link</div>
      </div>
      <div className="body-content">
        {toolList.map(({ logo, tool, owner, email, phone, link }, index) => {
          if(query){
            console.log("==== checking", owner.search(query))
            if((tool.search(query) > -1) || (owner.search(query) > -1 ) || (email.search(query) > -1) || (phone.search(query) > -1)){
              return (
                <div className="tool-wrap" key={`tool-count-${index}`}>
                  <div className="content sno">{index + 1}</div>
                  <Image
                    className="content logo"
                    src={logo}
                    alt={tool}
                    width={64}
                    height={64}
                    loader={({ src }) => src}
                    
                  />
                  <div className="content name">{tool}</div>
                  <div className="content owner">{owner}</div>
                  <div className="content email">{email}</div>
                  <Link href={link} className="content visit-link">
                  <Image
                    className="content logo"
                    src="https://images.mamaearth.in/png/external-link.png"
                    alt={tool}
                    width={64}
                    height={64}
                    loader={({ src }) => src}
                  />
                  </Link>
                </div>
              );
            } else {
              return null;
            }
          }

          return (
            <div className="tool-wrap" key={`tool-count-${index}`}>
              <div className="content sno">{index + 1}</div>
              <Image
                className="content logo"
                src={logo}
                alt={tool}
                width={64}
                height={64}
                loader={({ src }) => src}
                
              />
              <div className="content name">{tool}</div>
              <div className="content owner">{owner}</div>
              <div className="content email">{email}</div>
              <Link href={link} className="content visit-link">
              <Image
                className="content logo"
                src="https://images.mamaearth.in/png/external-link.png"
                alt={tool}
                width={64}
                height={64}
                loader={({ src }) => src}
              />
              </Link>
            </div>
          );
          
        })}
      </div>
    </DashboardContentLayout>
  );
};

export default DashboardContent;

'use client';

import styled from "styled-components";
import DashboardHeader from "../header";

interface DashboardMobileProps {
  isMobile: boolean
}

const DashboardLayout = styled.div``;

const DashboardMobile = ({ isMobile }: DashboardMobileProps) => {
  return (
    <DashboardLayout>
      <DashboardHeader isMobile={isMobile} />
    </DashboardLayout>
  );
};

export default DashboardMobile;

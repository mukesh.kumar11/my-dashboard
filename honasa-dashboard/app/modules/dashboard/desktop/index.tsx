"use client";
import styled from "styled-components";
import DashboardHeader from "../components/header";
import Image from "next/image";
import DashboardContent from "../components/content";
import { useState } from "react";

interface DashboardDesktopProps {
  isMobile: boolean;
}

const DashboardLayout = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 100vh;

  .left-sidebar {
    /* background-color: black; */
    min-width: 240px;
    display: flex;
    flex-direction: column;
    align-items: center;
    box-shadow: rgba(50, 50, 93, 0.25) 0px 13px 27px -5px, rgba(0, 0, 0, 0.3) 0px 8px 16px -8px;
    /* padding: 20px; */

    .separator {
      width: 100%;
      background-color: green;
      height: 1px;
      margin-top: 12px;
    }
  }

  .right-side-content {
    display: flex;
    flex-direction: column;
    width: 100%;
    align-items: center;
    /* padding: 24px; */

    .wrapper {
      display: flex;
      flex-direction: column;
      width: 97%;
      align-items: center;
      /* padding: 20px; */
      background-color: white;
      box-shadow: rgba(0, 0, 0, 0.24) 0px 3px 8px;
      margin-top: 24px;
      margin-left: 32px;
    }
  }
`;

const DashboardDesktop = ({ isMobile }: DashboardDesktopProps) => {
  const [query, setQuery] = useState("");
  const handleSearch = async (e: { target: { value: string } }) => {
    const { value } = e.target;
    setQuery(value);
  };

  return (
    <DashboardLayout>
      <div className="left-sidebar">
        <Image
          src="https://images.mamaearth.in/png/honasa-logo.png"
          alt="Honasa"
          width={128}
          height={100}
          style={{
            aspectRatio: 1686 / 1313,
            margin: "35px",
          }}
          loader={({ src }) => src}
        />
        <div className="separator" />
      </div>
      <div className="right-side-content">
        <DashboardHeader isMobile={isMobile} handleSearch={handleSearch}/>
        <div className="wrapper">
          <DashboardContent isMobile={isMobile} query={query}/>
        </div>
      </div>
    </DashboardLayout>
  );
};

export default DashboardDesktop;

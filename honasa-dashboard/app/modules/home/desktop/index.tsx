"use client";

import styled from "styled-components";
import Header from "../components/header";
import Content from "../components/content";

interface HomePageDesktopProps {
  isMobile: boolean;
}

const HeaderDesktopLayout = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
`;

const HomePageDesktop = ({ isMobile }: HomePageDesktopProps) => {
    console.log("===== checking HomePageDesktop-===", isMobile);
  return (
    <HeaderDesktopLayout>
      <Header isMobile={isMobile} />
      <Content isMobile={isMobile}/>
    </HeaderDesktopLayout>
  );
};

export default HomePageDesktop;

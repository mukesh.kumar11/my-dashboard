import { PrimaryButton } from "honasa/app/components/button/primary";
import Image from "next/image";
import styled from "styled-components";

interface ContentProps {
  isMobile: boolean;
}

const ContentLayout = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;

  .left-container {
    width: 50%;
    display: flex;
    flex-direction: row;
    justify-content: center;
  }

  .right-container {
    width: 50%;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 40px 10px 40px 120px;
    border-radius: 12px;
    box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
  }
`;

const Content = ({ isMobile }: ContentProps) => {
  return (
    <ContentLayout>
      <div className="left-container">
        <Image
          src="https://images.mamaearth.in/jpg/house-of-brands.jpg"
          width={800}
          height={500}
          alt="Honasa Brands"
          loader={({ src }) => src}
          style={{
            aspectRatio: 4802/3005,
            padding: "36px"
          }}
        />
      </div>
      <div className="right-container">
        <div className="title">
          Sign-in to Honasa Dashboard
        </div>
        <div className="button-container">
            <PrimaryButton  text="Sign in"/>
        </div>
      </div>
    </ContentLayout>
  );
};

export default Content;

"use client";
import React from "react";
import Image from "next/image";
import styled from "styled-components";

interface HeaderProps {
  isMobile: boolean;
}

const HeaderLayout = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 32px;
`;

const Header = ({ isMobile }: HeaderProps) => {
  return (
    <HeaderLayout>
      <Image
        src="https://images.mamaearth.in/png/honasa-logo.png"
        alt="Honasa"
        width={230}
        height={180}
        style={{
          aspectRatio: 1686/1313
        }}
        loader={({ src }) => src}
      />
    </HeaderLayout>
  );
};

export default Header;

import { headers } from "next/headers";
import HomeDesktop from "./modules/home/desktop";
import HomeMobile from "./modules/home/mobile";

const HomePage = (): JSX.Element => {
  const headerlist = headers();
  const isMobile = headerlist.get("isMobile") == "1" ? true : false;

  if (isMobile) {
    return <HomeMobile />;
  }

  return <HomeDesktop isMobile={isMobile}/>;
};



export default HomePage;

import type { Metadata } from "next";
import { Open_Sans } from "next/font/google";
import "./globals.css";
import { headers } from "next/headers";
import StyledComponentsRegistry from "./lib/registry";
// import Header from "./modules/home/components/header";
const openSans = Open_Sans({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Honasa Consumer Limited",
  description: "Honasa Consumer Limited",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  const headerlist = headers();
  const isMobile = headerlist.get("isMobile") == "1" ? true : false;

  return (
    <html lang="en">
      <body className={openSans.className}>
        <StyledComponentsRegistry>
          {children}
          </StyledComponentsRegistry>
      </body>
    </html>
  );
}
